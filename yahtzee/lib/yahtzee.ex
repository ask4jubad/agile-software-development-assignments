defmodule Yahtzee do
  @moduledoc """
  Documentation for Yahtzee.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Yahtzee.hello()
      :world

  """
  def hello do
    :world
  end
    #def score_upper(_dice) do
    #  %{Ones: 1}
    #end
  def score_upper(dice) do
    %{Ones: length(Enum.filter(dice, fn e -> e == 1 end)),
    Twos: length(Enum.filter(dice, fn f -> f == 2 end)),
    Threes: length(Enum.filter(dice, fn g -> g == 3 end)),
    Fours: length(Enum.filter(dice, fn h -> h == 4 end)),
    Fives: length(Enum.filter(dice, fn i -> i == 5 end)),
    Sixes: length(Enum.filter(dice, fn e -> e == 6 end))
    }
  end

  def score_lower(dices) do
    %{"Three of a kind": three_of_a_kind(dices),
    "Four of a kind": four_of_a_kind(dices),
    "Full house": full_house(dices),
    "Small straight": small_straight(dices),
    "Large straight": large_straight(dices),
    Yahtzee: yahtzee(dices),
    Chance: chance(dices)
    }
  end

  def three_of_a_kind(dices) do
    threeofakind= Enum.uniq(dices) |>Enum.flat_map(fn x-> [Enum.count(dices, fn b -> x == b end)] end)
    if Enum.sort(threeofakind) == [1,1,3]
        do Enum.sum(dices)
    end
  end

  def four_of_a_kind(dices) do
    fourofakind= Enum.uniq(dices) |>Enum.flat_map(fn x-> [Enum.count(dices, fn b -> x == b end)] end)
    if Enum.sort(fourofakind) == [1,4]
      do Enum.sum(dices)
    end
  end

  def full_house(dices) do
    fullHouse = Enum.uniq(dices) |> Enum.flat_map(fn x-> [Enum.count(dices, fn b -> x == b end)] end)
    if Enum.sort(fullHouse) == [2,3]
      do 25
    end
  end

  def small_straight(dices) do
    smallStraight = Enum.sort(Enum.uniq(dices))
    if smallStraight == [1,2,3,4] || smallStraight == [2,3,4,5] || smallStraight == [3,4,5,6]
      do 30
    end
  end

  def large_straight(dices) do
    largeStraight = Enum.sort(dices)
    if largeStraight == Enum.to_list(1..5) || largeStraight == Enum.to_list(2..6)
      do 40
    end
  end

  def yahtzee(dices) do
    yahtzeepoint = length(Enum.uniq(dices))
    if yahtzeepoint == 1
      do 50
    end
  end

  def chance(dices) do
    chancepoint= Enum.sort(Enum.uniq(dices)|> Enum.flat_map(fn x-> [Enum.count(dices, fn b -> x == b end)] end))
    if chancepoint == [1,2,2] do
      Enum.sum(dices)
    end
  end

end
