{application,hangman,
             [{applications,[kernel,stdlib,elixir,logger,cowboy,plug,
                             dictionary]},
              {description,"hangman"},
              {modules,['Elixir.Game','Elixir.GameRouter','Elixir.Hangman']},
              {registered,[]},
              {vsn,"0.1.0"}]}.
