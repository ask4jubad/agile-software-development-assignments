defmodule Hangman do

    def score_guess(state, guess) do
  
       trials = remaining_turns (state)
       case trials == 0 do
        true -> state
        false -> case String.contains?(secret_word(state),guess) do
                        true -> case String.contains?(correct_guesses(state),guess) do
                                  true -> {secret_word(state), correct_guesses(state), wrong_guesses(state), trials}
                                  false -> {secret_word(state), Enum.join([correct_guesses(state),guess]), wrong_guesses(state), (trials-1)}
                                end
  
                        false -> case String.contains?(wrong_guesses(state),guess) do
                                    true -> {secret_word(state), correct_guesses(state), wrong_guesses(state), trials}
  
                                    false -> {secret_word(state), correct_guesses(state), Enum.join([wrong_guesses(state),guess]), (trials-1)}
                                  end
                  end
       end
    end
  
    def format_feedback(state) do
      Enum.join(Enum.map(String.codepoints(secret_word(state)), fn g ->  if String.contains?(correct_guesses(state), g) do g else "-" end end))
    end
  
    def secret_word(state) do
       elem(state,0)
    end
  
    def correct_guesses(state) do
      elem(state,1)
    end
  
    def wrong_guesses(state) do
      elem(state,2)
    end
  
    def remaining_turns (state) do
      elem(state,3)
  
    end
  
  end
  
  
   # countuniqletters = String.to_charlist (:secret_word)
      # traverselist = Enum.uniq(countuniqletters)
      # remainingturns =
  