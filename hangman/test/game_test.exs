defmodule GameTest do
    use ExUnit.Case
    import Mock

    test "Creates a process holding the game state" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert is_pid(pid)
        assert pid != self()
        assert called Dictionary.random_word
      end
    end

    test "Initialize secret word" do
      assert {:ok, {"platypus","","",9}} = Game.init("platypus")
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert is_pid(pid)
        assert pid != self()
        assert called Dictionary.random_word
        
      end 
    end

    test "Still in play" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        {:ok, {"platypus","","",9}} = Game.init("platypus")
        assert :ok = Game.submit_guess(pid,"y")
        assert {:noreply, {"platypus", "y", "", 8}}= Game.handle_cast({:submit_guess, "y"}, {"platypus","","",9})
        assert %{feedback: "----y---", remaining_turns: 8, status: :playing}= Game.get_feedback(pid) 
      end
    end

    test "Won with Correct guesses" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert {:ok, {"platypus","","",9}} = Game.init("platypus")
        Enum.each(~w[p l a t y u s], fn(letter) -> assert :ok = Game.submit_guess(pid, letter) end)
        assert %{feedback: "platypus", remaining_turns: 2, secret: "platypus", status: :win}= Game.get_feedback(pid)
      end
    end

    test "Wrong Guesses" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert {:ok, {"platypus","","",9}} = Game.init("platypus")
        Enum.each(~w[p l a t y], fn(letter) -> assert :ok = Game.submit_guess(pid, letter) end)
        Enum.each(~w[q r v m], fn(letter) -> assert :ok = Game.submit_guess(pid, letter) end)
        assert %{feedback: "platyp--", remaining_turns: 0, secret: "platypus", status: :lose}= Game.get_feedback(pid) 
      end   
    end

end