defmodule GameRouterTest do
    use ExUnit.Case
    use Plug.Test
  
    @opts GameRouter.init([])
  
    test "Reports path / is not served by the application" do
      conn = conn(:get, "/")
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 404
      assert conn.resp_body == "Oops"
    end

    test "Create games" do
      GameRouter.start_link()
      conn = conn(:post, "/games")
      conn = GameRouter.call(conn, @opts)
      IO.inspect(conn)
      assert conn.status == 201
      assert conn.resp_body == "Your game has been created"
    end

    test "where game is unsuccessful" do
      GameRouter.start_link()
      conn = conn(:get, "/games/:id")
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 404
      assert conn.resp_body == "Game URL is unknown"
    end

    test "where game is successful" do
      GameRouter.start_link()
      id = UUID.uuid1()
      {:ok, pid} = Game.start_link(String.to_atom(id))
      conn = conn(:get, "/games/#{id}")
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 200
      assert is_map(Poison.decode!(conn.resp_body)) == true
    end

    test "Post guesses in games" do
      GameRouter.start_link()
      id = UUID.uuid1()
      {:ok, pid} = Game.start_link(String.to_atom(id))
       conn = conn(:post, "/games/#{id}/guesses", %{"guess"=> "p"})
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 201
      #assert conn.resp_body == "Game URL is unknown"
    end

    
    test "Post wrongguesses in games" do
      GameRouter.start_link()
      id = UUID.uuid1()
      {:ok, pid} = Game.start_link(String.to_atom(id))
       conn = conn(:post, "/games/1/guesses", %{"guess"=> "p"})
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 404
      assert conn.resp_body == "Game URL is unknown"
    end

  end