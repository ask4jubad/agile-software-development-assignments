Feature: Auction Price Update
  As a customer
  Such that I go to Auctions page
  I want to bid offer

  Scenario: Offer and Update Price accordingly
    Given the following bids are open
          | Description                             | Price	  | Closing Date    | Action |
          | Roller Derby Brond Blade Skate (Size 7) | 29.00   | 09/01/2019	   | Select |
          | Chicago Bullet Speed Skate (Size 7)     | 59.00   | 09/01/2019	   | Select |
          | Riedell Dart Derby Skate (Size 8)       | 106.00  | 09/01/2019	   | Select |
    And I want to bid "62.00" for "Chicago Bullet Speed Skate (Size 7)"
    And I open Items web page
    And I select the Item
    When I submit the offer
    Then I should receive a confirmation message

    Scenario: Offer and the Bid is Rejected
    Given the following bids are open
          | Description                             | Price	  | Closing Date    | Action |
          | Roller Derby Brond Blade Skate (Size 7) | 29.00   | 09/01/2019	   | Select |
          | Chicago Bullet Speed Skate (Size 7)     | 59.00   | 09/01/2019	   | Select |
          | Riedell Dart Derby Skate (Size 8)       | 106.00  | 09/01/2019	   | Select |
    And I want to bid "50.00" for "Riedell Dart Derby Skate (Size 8)"
    And I open Items web page
    And I select the Item
    When I submit the offer
    Then I should receive a rejected message