defmodule PrepWeb.PageController do
  use PrepWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
