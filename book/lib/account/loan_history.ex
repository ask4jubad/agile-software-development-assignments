defmodule Book.Account.LoanHistory do
  use Ecto.Schema
  import Ecto.Changeset
  alias Book.Account.User
  alias Book.Account.Book
  
  schema "loans" do
    field :start_date, :date
    field :due_date, :date
    field :cost, :float
    field :status, :string
    field :extended, :integer
    belongs_to :user, User
    belongs_to :book, Book
    timestamps()
  end

  @doc false
  def changeset(loan_history, attrs) do
    loan_history
    |> cast(attrs, [:start_date, :due_date, :cost, :status, :extended, :user_id, :book_id])
    |> validate_required([[:start_date, :due_date, :cost, :status, :extended, :user_id, :book_id]])
  end
end
