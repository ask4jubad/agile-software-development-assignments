defmodule Book.Account.Book do
  use Ecto.Schema
  import Ecto.Changeset
  alias Book.Account.LoanHistory
  

  schema "books" do
      field :code, :string
      field :title, :string
      field :author, :string
      has_many :loan_history,  LoanHistory
    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:code, :title, :author])
    |> validate_required([:code, :title, :author])
  end
end
