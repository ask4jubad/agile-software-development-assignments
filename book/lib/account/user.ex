defmodule Book.Account.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Book.Account.LoanHistory
  

  schema "users" do
    field :username, :string
    has_many :loan_history, LoanHistory
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username])
    |> validate_required([:username])
  end
end
