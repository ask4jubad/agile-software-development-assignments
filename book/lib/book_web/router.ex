defmodule BookWeb.Router do
  use BookWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BookWeb do
    pipe_through :browser # Use the default browser stack

    # get "/", PageController, :index
    resources "/book", BookController
    get "/", BookController, :index
    post "/book/:id", BookController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", BookWeb do
  #   pipe_through :api
  # end
end
