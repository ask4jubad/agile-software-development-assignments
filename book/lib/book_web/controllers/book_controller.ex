defmodule BookWeb.BookController do
  use BookWeb, :controller
  alias Book.{Repo, Accounts.Book, Loan, Accounts.LoanHistory, Accounts.User}


  def index(conn, _params) do
    books =  Loan.get_my_books!(1)|> Repo.all |> Repo.preload(:book)
    render(conn, "index.html", books: books)
  end
  
  def show(conn, %{"id" => id}) do
    result = Loan.get_loan!(id)
    # coder = Loan.get_my_books!(1)|> Repo.all |> Repo.preload(:book)
    if result.code == "A" do  
    extension = %{:due_date => Date.add(result.due_date, 7), :extended => result.extended+1}
    else 
    if  result.code == "B" do
      extension = %{:due_date => Date.add(result.due_date, 14), :extended => result.extended+1}
    else
      extension = %{:due_date => Date.add(result.due_date, 21), :extended => result.extended+1} 
    
    case (result.extended < 4) do
     true ->
      with {:ok, %LoanHistory{} = _loan} <- Loan.update_loan(result, extension) do
      conn |> put_flash(:info, "Extension accepted")
          |> redirect(to: "/")
      end
     false ->
      conn
       |> put_flash(:error, "Extension rejected")
       |> redirect(to: "/")
    end
  end
end
end
end