defmodule Book.Loan do

  import Ecto.Query, warn: false
  alias Book.Repo

  # alias Vueshatter.Models.LoanHistory
  alias Book.Account.Book
  alias Book.Account.User
  alias Book.Account.LoanHistory


  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end
 
  def create_books(attrs \\ %{}) do
    %Book{}
    |> Book.changeset(attrs)
    |> Repo.insert()
  end

  def create_loans(attrs \\ %{}) do
    %LoanHistory{}
    |> LoanHistory.changeset(attrs)
    |> Repo.insert()
  end

  def select_mybooks(accounts, my_id) do
    from l in accounts,
    join: b in Book,
    on: b.id == l.book_id,
    where: l.user_id == ^my_id,
    select: l
  end

  def get_my_books!(my_id) do
      from orders  in LoanHistory , where: orders.user_id == ^my_id
   end

  def my_books!(my_id, id), do: Repo.get_by(LoanHistory, user_id: my_id, book_id: id)

  def get_loan!(id), do: Repo.get!(LoanHistory, id)

  # def update_loan(%LoanHistory{} = loan, attrs) do
  #   loan
  #   |> LoanHistory.changeset(attrs)
  #   |> Repo.update()
  # end
end
