# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :book,
  ecto_repos: [Book.Repo]

# Configures the endpoint
config :book, BookWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "fxgXtm77az4O4ZQF2FuL7/k5d3guLw9uWlUz3MKp9yCrveu2+A15ycgiN99CuWhN",
  render_errors: [view: BookWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Book.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# config :hound, driver: "chrome_driver"
# config :takso, sql_sandbox: true 

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
