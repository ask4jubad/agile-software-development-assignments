defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Book.{Repo, Item}
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Book.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Book.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Book.Repo)
    Hound.end_session
  end 

  given_ ~r/^the following books are available$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I want to extend the "(?<argument_one>[^"]+)" by respective days$/,
  fn state, %{book_name: book_name} ->
    state = Map.put(state, :book_name, book_name)
    {:ok, state}
  end

  and_ ~r/^I open book loans web page$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end

  when_ ~r/^extend button is clicked$/, fn state ->
    row = find_all_elements(:tag, "tr")|> Enum.find(&(visible_text(&1)=~state[:book_name]))
    find_within_element(row, :class, "btn-default") |> click
    {:ok, state}
  end

  then_ ~r/^The system checks that extension count is not at its limit and I should receive flash message of rejection$/, fn state ->
    assert visible_in_page? ~r/Extention accepted/
    {:ok, state}
  end

end
