Feature: Libarary Book Borrowing
  As a student
  Such that I have burrowed book
  And I want to Extend it

  Scenario: Borrowing with extension acceptance
    Given the following books are available
          | title                      | Code | start_date | due_date   |
          | Phoenix Programming 1.3    |   A  | 2019-01-10 | 2019-02-17 |
          | Secrets of Javascript ninja|   A  | 2019-01-10 | 2019-02-17 |
    And I want to extend the "Secrets of Javascript ninja" by respective days
    And I open book loans web page
    When Extend button is clicked
    Then The system checks that extension count is not at its limit and I should receive flash message of acceptance

Scenario: Borrowing with extension 
    Given the following books are available
          | title                      | Code | start_date | due_date   |
          | Phoenix Programming 1.3    |   A  | 2019-01-10 | 2019-02-17 |
          | Secrets of Javascript ninja|   A  | 2019-01-10 | 2019-02-17 |
    And I want to extend the "Secrets of Javascript ninja" by respective days
    And I open book loans web page
    When extend button is clicked
    Then The system checks that extension count is not at its limit and I should receive flash message of rejection
