defmodule Book.Repo.Migrations.Books do
  use Ecto.Migration

  def change do
    create table(:books) do
      add :code, :string
      add :title, :string
      # add :author, :string
      timestamps()
  end
end
