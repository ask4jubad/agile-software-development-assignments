# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Book.Repo.insert!(%Book.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.


alias Book.Loan

user = [%{username: "Jubril"},%{username: "Naveed"}]
Enum.each(user, fn(data) -> Loan.create_user(data) end)


  books = [
    %{
       title: "Phoenix Programming 1.3",
       code: "A"
    #   author: "asfer"
    },
    %{
        title: "The secret of Javascript",
        code: "A"
    #   author: "jninfi"
    }

  ]
Enum.each(books, fn(data) ->
Loan.create_books(data)
  end)


loans = [
    %{
    start_date: Date.utc_today,
    due_date: if books.code == "A" do Date.add(Date.utc_today, 7) else if books.code == "B" do Date.add(Date.utc_today, 14) else  Date.add(Date.utc_today, 21) end do
    cost: 10.00,
    status: "Loan",
    extended: 0,
    user_id: 1,
    book_id: 1
    },
    %{
      start_date: Date.utc_today,
      due_date: if books.code == "A" do Date.add(Date.utc_today, 7) else if books.code == "B" do Date.add(Date.utc_today, 14) else  Date.add(Date.utc_today, 21) end do
    #   due_date: Date.add(Date.utc_today, 7),
      cost: 10.00,
      status: "Loan",
      extended: 0,
      user_id: 1,
      book_id: 2
    }

  ]

  Enum.each(loans, fn(data) ->
    Loan.create_loans(data)
      end)
