
import "phoenix_html"

import Vue from "vue";
new Vue({
  el: "index",
  data: {
    message: "Library book borrowing"
  }
});

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"
