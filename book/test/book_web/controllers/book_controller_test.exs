defmodule BookWeb.BookControllerTest do
  use BookWeb.ConnCase
  alias Book.Accounts.{User, LoanHistory, Book, Repo}
  
    test "POST /extension (acceptance)", %{conn: conn} do
        [%{description: "Programming Phoenix 1.3", code: "A", start_date: Ecto.Date.cast!("2019-01-10"), due_date: Ecto.Date.cast!("2018-01-17"), extended: 0}]
        |> Enum.map(fn book_data -> Book.changeset(%Book{}, book_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
        Book = Repo.get_by!(Book, description: "Programming Phoenix 1.3")
        conn = post conn, "/book", %{offer: [id: Book.id, code: "A"]}
        conn1 = get conn, redirected_to(conn)
        assert html_response(conn1, 200) =~ ~r/Extension accepted/
    end
    test "POST /extension (rejection)", %{conn: conn} do
        [%{description: "Secrets of the JavaScript ninja", code: "A", start_date: Ecto.Date.cast!("2019-01-10"), due_date: Ecto.Date.cast!("2019-01-17"), extended: 0}]
        |> Enum.map(fn book_data -> Book.changeset(%Book{}, book_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
        Book = Repo.get_by!(Book, description: "Secrets of the JavaScript ninja")
        conn = post conn, "/book", %{offer: [id: Book.id, code: "A"]}
        conn1 = get conn, redirected_to(conn)
        assert html_response(conn1, 200) =~ ~r/Extension rejected/
    end
end
