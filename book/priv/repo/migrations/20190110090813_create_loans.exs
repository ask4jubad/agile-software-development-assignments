defmodule Book.Repo.Migrations.Loans do
  use Ecto.Migration

  def change do
    create table(:loans) do
      add :start_date, :date
      add :due_date, :date
      add :cost, :float
      add :status, :string
      add :extended, :integer, default: 0, null: false
      add :user_id, references(:users)
      add :book_id, references(:books)
    
      timestamps()
    end
    create index(:loans, [:user_id])
    create index(:loans, [:book_id])
  end
end
